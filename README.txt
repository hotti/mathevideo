Die aktuellste Version liegt im develop-Branch, eine stabile Version auf dem main. 
Der Code für die UI steht in "ik_ui.m".
Wesentlicher Code für die Motoransteuerung steht in "DynamixelSDK-master\matlab\protocol2.0\two_angels.m"
Um den Code auszuführen, müssen in Matlab die Dependencies für das Dynamixel SDK definiert und die Robotics System Toolbox installiert sein, sowie die relevanten Konstanten in "two_angels.m" angepasst werden, falls Motoren vorhanden sind. Ansonsten funktioniert die UI auch eigenständig, wenn auch mit fragwürdigem Error-Handling. 
Eine explizite Anleitung für das Dynamixel SDK gibt es auch hier: 
https://www.youtube.com/watch?v=1VWksgPqyDs&t=84s
